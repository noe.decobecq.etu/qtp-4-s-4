package src.test.java.fr.univlille.iut.r402.fxview;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import src.main.java.fr.univlille.iut.r402.domain.Pizza;
import src.main.java.fr.univlille.iut.r402.domain.PizzaRecette;
import src.main.java.fr.univlille.iut.r402.domain.Pizzaiolo;
import src.main.java.fr.univlille.iut.r402.domain.PizzasOven;


public class PizzaioloTest {
    @Test
    public void pizzaiolo_should_make_pizza_and_add_it_in_the_oven() {
        Pizzaiolo pizzaiolo = new Pizzaiolo();
        PizzasOven pizzasOven = new PizzasOven();
        Pizza margherita = pizzaiolo.makePizza(PizzaRecette.Margherita);
        pizzaiolo.cookIn(margherita, pizzasOven);
        assertEquals(1, pizzasOven.getNumberOfPizzas());
    }
    
}
