package src.main.java.fr.univlille.iut.r402.domain;

public interface Observer {
	public void update(Subject subj);
	public void update(Subject subj, Object data);
}