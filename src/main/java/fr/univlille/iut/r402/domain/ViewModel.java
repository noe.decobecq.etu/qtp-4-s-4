package src.main.java.fr.univlille.iut.r402.domain;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ViewModel implements Observer {
	protected int pizzasNumber = 0;
	protected StringProperty numberOfPizzasInOven = new SimpleStringProperty();
	protected Pizzaiolo pizzaiolo; 
	protected PizzasOven pizzasOven;
	
	public ViewModel(Pizzaiolo pizzaiolo, PizzasOven pizzasOven) {
		this.pizzaiolo = pizzaiolo;
		this.pizzasOven = pizzasOven;
		pizzasNumber=pizzasOven.getNumberOfPizzas();
		pizzasOven.attach(this);
	}

	@Override
	public void update(Subject subj) {
		pizzasNumber++;
		numberOfPizzasInOven.set(String.valueOf(pizzasNumber));
		subj.notifyObervers();
	}

	@Override
	public void update(Subject subj, Object data) {
		pizzasNumber += (int)data;
		numberOfPizzasInOven.set(String.valueOf(pizzasNumber));
		subj.notifyObservers(data);
	}
}
