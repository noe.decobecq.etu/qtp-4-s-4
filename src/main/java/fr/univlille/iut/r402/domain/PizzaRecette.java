package src.main.java.fr.univlille.iut.r402.domain;

public enum PizzaRecette {
	Margherita("Margherita");
	private String name;
	PizzaRecette(String string) {
		this.name=string;
	}
	
	public String toString() {
		return this.name;
		
	}
	
	
}
