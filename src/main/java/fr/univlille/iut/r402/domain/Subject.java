package src.main.java.fr.univlille.iut.r402.domain;

import java.util.ArrayList;

public abstract class Subject {
	private ArrayList<Observer> observers = new ArrayList<Observer>();

	protected void notifyObervers() {
		for(Observer o : observers) {
			o.update(this);
		}
	}

	protected void notifyObservers(Object data) {
		for(Observer o : observers) {
			o.update(this, data);
		}
	}

	public void attach(Observer observer) {
		observers.add(observer);
	}

	public void detach(Observer observer) {
		observers.remove(observer);
	}
}