package src.main.java.fr.univlille.iut.r402.domain;

public class Pizzaiolo {
	public void cookIn(Pizza p, PizzasOven four) {
		four.bake(p);
	}
	
	public Pizza makePizza(PizzaRecette p) {
		return new Pizza(p.toString());
	}
}
