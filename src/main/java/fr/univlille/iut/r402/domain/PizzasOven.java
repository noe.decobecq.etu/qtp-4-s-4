package src.main.java.fr.univlille.iut.r402.domain;

import java.util.ArrayList;

public class PizzasOven extends Subject{
	protected ArrayList<Pizza> pizzas = new ArrayList<Pizza>();
	
	public void bake(Pizza margherita) {
		pizzas.add(margherita);
	}
	
	public int getNumberOfPizzas() {
		return pizzas.size();
	}
	
}
