package src.main.java.fr.univlille.iut.r402.fxview;


import javafx.application.Application;
import javafx.stage.Stage;
import src.main.java.fr.univlille.iut.r402.domain.PizzaRecette;
import src.main.java.fr.univlille.iut.r402.domain.Pizzaiolo;
import src.main.java.fr.univlille.iut.r402.domain.PizzasOven;

public class Main extends Application {
    @Override
    public void start(Stage stage) {
        // création du four et du cuisinier  : Les modèles
        PizzasOven pizzasOven = new PizzasOven();
        Pizzaiolo pizzaiolo = new Pizzaiolo();
        pizzaiolo.cookIn(pizzaiolo.makePizza(PizzaRecette.Margherita), pizzasOven);
        // création du model de vue
        PizzeriaViewModel pizzeriaViewModel = new PizzeriaViewModel(pizzaiolo, pizzasOven);
        
        // création de la vue javafx
        PizzeriaView pizzeriaView = new PizzeriaView(pizzeriaViewModel);
        // lancement de l'écran
        pizzeriaView.start();
    }
}
