package src.main.java.fr.univlille.iut.r402.fxview;

import src.main.java.fr.univlille.iut.r402.domain.Pizzaiolo;
import src.main.java.fr.univlille.iut.r402.domain.PizzasOven;
import src.main.java.fr.univlille.iut.r402.domain.ViewModel;

public class PizzeriaViewModel extends ViewModel{

	public PizzeriaViewModel(Pizzaiolo pizzaiolo, PizzasOven pizzasOven) {
		super(pizzaiolo, pizzasOven);
	}

	public int getPropertyNumberOfPizzasInOven() {
		// TODO Auto-generated method stub
		return this.pizzasNumber;
	}
}
