package src.main.java.fr.univlille.iut.r402.fxview;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class PizzeriaView {
    private PizzeriaViewModel viewModel;

    public PizzeriaView(PizzeriaViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void start() {
        Stage stage = new Stage();
        Label label = new Label();
        label.setPrefWidth(300);
        label.setText(viewModel.getPropertyNumberOfPizzasInOven()+"");
        Button b = new Button("add");
        b.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
        });
        Scene scene = new Scene(new HBox(label,b));
        stage.setScene(scene);
        stage.show();
    }
}
