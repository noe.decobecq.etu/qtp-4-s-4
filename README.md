# Pizzeria

Nous allons faire un jeu de simulation de pizzeria.

L'architecture que nous souhaitons avoir est la suivante : 

![](./img/mmvc-pizzeria.svg)

Nous allons bien évidement tout écrire en TDD.

# Codons
Ajouter la dépendance javafx dans le pom

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-maven-plugin</artifactId>
            <version>0.0.8</version>
            <configuration>
                <mainClass>HelloFX</mainClass>
            </configuration>
        </plugin>
    </plugins>
</build>
```
```xml
<dependencies>
  <dependency>
    <groupId>org.openjfx</groupId>
    <artifactId>javafx-controls</artifactId>
    <version>19</version>
  </dependency>
</dependencies>
```

Ajouter la dépendance JUnit dans le pom

```xml
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.junit</groupId>
            <artifactId>junit-bom</artifactId>
            <version>5.9.2</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

et 

```xml
<dependencies>
    <dependency>
        <groupId>org.junit.platform</groupId>
        <artifactId>junit-platform-suite</artifactId>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.junit.jupiter</groupId>
        <artifactId>junit-jupiter</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>
```

# Un affichage de bout en bout

Le premier test va nous permettre plusieurs choses :

* pour le domain : Un `Pizzaiolo` est capable de `fabriquer` des `Pizza` et de les `mettre à cuire` dans un `Four à Pizzas`
* pour le modèle de vue javafx : il existe une propriété perméttant de voir le nombre de pizzas dans le four
* fait émerger un pattern Observer / Observable avec un sujet `StringSubject` et `ViewModel` l'interface observatrice
* Petite particularité sur laquelle ce test n'est pas explicite : `public javafx.beans.property.StringProperty getPropertyNumberOfPizzasInOven() {` ce qui implique `private StringProperty numberOfPizzasInOven = new SimpleStringProperty();`

```java
public class PizzeriaViewModelTest {
    @Test
    void should_display_the_number_of_pizzas_in_the_oven_i_e_PizzaioloViewModel_can_observe_PizzasOven_andPizzaOven_is_observable() {
        Pizzaiolo pizzaiolo = new Pizzaiolo();
        Pizza margherita = pizzaiolo.makePizza(PizzaRecette.Margherita);
        StringSubject pizzasOven = new PizzasOven();
        pizzaiolo.cookIn(margherita, (PizzasOven) pizzasOven);
        ViewModel pizzaioloViewModel = new PizzeriaViewModel(pizzaiolo, (PizzasOven) pizzasOven);
        assertEquals("1", ((PizzeriaViewModel) pizzaioloViewModel).getPropertyNumberOfPizzasInOven().getValue());
    }
}
```
Faire passer ce test au rouge i.e. créer toutes les classes et les interfaces pour que ça compile.


Désactiver ce premier test beaucoup trop gros pour être implémenté rapidement et ajouter ce test en double boucle qui ne concerne que le domaine

```java
public class PizzaioloTest {
    @Test
    void pizzaiolo_should_make_pizza_and_add_it_in_the_oven() {
        Pizzaiolo pizzaiolo = new Pizzaiolo();
        PizzasOven pizzasOven = new PizzasOven();
        Pizza margherita = pizzaiolo.makePizza(PizzaRecette.Margherita);
        pizzaiolo.cookIn(margherita, pizzasOven);
        assertEquals(1, pizzasOven.getNumberOfPizzas());
    }
}
```

Faire passer ce test au vert (il faudrat probablement créer une méthode `public void bake(Pizza margherita) {` sur la classe `PizzasOven`

Réactiver le premier test et faire passer tous les tests au vert. Il faudra pour ça attacher la vue `PizzeriaViewModel` au model `PizzasOven` dès sa construction et implémenter la partie testé du pattern observer / observable

N'oubliez pas la phase de restructuration.

Il est temps de créer notre premier écrant, ajouter la classe suivante : 
```java
public class PizzeriaView {
    private PizzeriaViewModel viewModel;

    public PizzeriaView(PizzeriaViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void start() {
        Stage stage = new Stage();
        TextField label = new TextField();
        label.setPrefWidth(300);
        label.textProperty().bindBidirectional(viewModel.getPropertyNumberOfPizzasInOven());
        Scene scene = new Scene(new HBox(label));
        stage.setScene(scene);
        stage.show();
    }
}
```

Ainsi que la classe `Main`

```java
public class Main extends Application {
    @Override
    public void start(Stage stage) {
        // création du four et du cuisinier  : Les modèles
        PizzasOven pizzasOven = new PizzasOven();
        Pizzaiolo pizzaiolo = new Pizzaiolo();
        // création du model de vue
        PizzeriaViewModel pizzeriaViewModel = new PizzeriaViewModel(pizzaiolo, pizzasOven);
        // création de la vue javafx
        PizzeriaView pizzeriaView = new PizzeriaView(pizzeriaViewModel);
        // lancement de l'écran
        pizzeriaView.start();
    }
}
```

Pour lancer l'application rendez-vous dans un terminal `mvn javafx:run`

Ajouter le test suivant :

```java
@Test
void should_update_the_number_of_pizzas_in_the_oven() {
    Pizzaiolo pizzaiolo = new Pizzaiolo();
    Pizza margherita = pizzaiolo.makePizza(PizzaRecette.Margherita);
    PizzasOven pizzasOven = new PizzasOven();
    pizzaiolo.cookIn(margherita, pizzasOven);
    PizzeriaViewModel pizzeriaViewModel = new PizzeriaViewModel(pizzaiolo, pizzasOven);
    Pizza reine = pizzaiolo.makePizza(PizzaRecette.Reine);
    pizzaiolo.cookIn(reine, pizzasOven);
    assertEquals("2", pizzeriaViewModel.getPropertyNumberOfPizzasInOven().getValue());
}
```

Faire passer ce test au vert

N'oubliez pas la phase de restructuration, les tests ont permit de faire émerger le code, mais il est important de les rendre simple et lisible.

Ajouter le test suivant : 

```java
@Test
void should_add_pizza_in_oven_from_UI() {
    PizzasOven pizzasOven = new PizzasOven();
    Pizzaiolo pizzaiolo = new Pizzaiolo();
    PizzeriaViewModel pizzeriaViewModel = new PizzeriaViewModel(pizzaiolo, pizzasOven);
    pizzeriaViewModel.cookNewReine();
    assertEquals(1, pizzasOven.getNumberOfPizzas());
}
```
Le faire passer au vert, restructurer puis modifier la vue javafx pour avoir un bouton "Faire une pizza Reine" branché sur la méthode `pizzeriaViewModel.cookNewReine()`

Ajouter le test suivant pour ajouter l'attribut `private BooleanProperty enableCookNewReine = new SimpleBooleanProperty();` dans la classe `PizzeriaViewModel`

```java
@Test
void should_disable_plus_button_when_oven_is_full() {
    PizzasOven pizzasOven = new PizzasOven();
    Pizzaiolo pizzaiolo = new Pizzaiolo();
    PizzeriaViewModel pizzeriaViewModel = new PizzeriaViewModel(pizzaiolo, pizzasOven);
    pizzeriaViewModel.cookNewReine();
    pizzeriaViewModel.cookNewReine();
    pizzeriaViewModel.cookNewReine();
    assertEquals(true, pizzeriaViewModel.getPropertyEnableCookNewReine().getValue());
}
```

Faire passer le test au vert puis brancher la `BooleanProperty` sur le bouton "Faire une pizza Reine", ça doit donner un truc comme `cookNewReine.disableProperty().bindBidirectional(viewModel.getPropertyEnableCookNewReine());` il faudra sans doute passer par la création d'une méthode `isFull()` sur `PizzasOven`.

Restructurez.

Ajouter de nouvelles fonctionnalités en TDD en respectant le MMVC. Vous pouvez vérifier le taux de couverture technique en utilisant la commande `mvn install site surefire-report:report` avant d'ouvrir la page `x-www-browser target/site/jacoco/index.html` Il n'y a que la classe `PizzeriaView` qui n'est pas testé toutes les autres doivent être à 100%.

Fonctionnalités à ajouter :
* Chaque pizza a maintenant un UUID, changer l'affichage pour avoir maintenant le contenu du four (les UUIDs des pizzas)
* Ajouter un bouton pour sortir toutes les pizzas cuites du four `pizzaiolo.cookOut(pizzasOven)`;
* Il faut cliquer sur un bouton pour vendre une pizza cuite, affichez le bouton "Vendre une pizza" 
* Ajouter le montant de la caisse
* Une pizza cuite depuis plus de 60s est invendable, ajouter un bouton "Mettre à la poubelle"
* maintenant que vous avez des sous, il va falloir acheter les ingrédients, ajouter l'achat des ingrédients de base pour faire vos différentes pizzas, il n'est alors possible de faire une pizza que si les ingrédients sont en stock.
* Ajouter l'achat d'un four plus grand
* Ajouter des prix différents en fonction des pizzas, avec des prix différents pour les ingrédients.
* Ajouter le fait que les ingrédients ne peuvent pas se garder indéfiniment.
* etc.

